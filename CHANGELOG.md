# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added

### Changed


## [0.2.0] - 2016-11-21
### Added
- Add property enableDownloadFromICloud to SynqUploader class.
- Add export progress callback to function uploadVideoArray.
- Add fully functional example project (requires an API key from synq.fm)

### Changed
- Removed success and failure callback from function uploadVideoArray 


## [0.1.0] - 2016-11-15
### Added
- This is the initial version.

